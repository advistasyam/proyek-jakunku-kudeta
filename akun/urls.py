from django.urls import path
from . import views

app_name = 'akun'

urlpatterns = [
    path('',views.user, name="user"),
    path('<username>',views.user_profile),
    path('<username>/edit',views.edit_profile),
]