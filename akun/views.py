from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
import json
from aplikasiutama.models import *
from .forms import EditProfileForm
# Create your views here.

def user(request):
	mhs = Mahasiswa.objects.all()
	return render(request, 'trialerrordifoldersiniaja/list_user.html',{'mhs':mhs})

def user_profile(request, username):
	usr = User.objects.get(username=username)
	mhs = Mahasiswa.objects.get(user=usr)
	form = EditProfileForm(request.POST or None, instance = request.user)
	
	if request.method == 'POST':
		print(request.POST)
		mhs.size_punya = request.POST['spunya']
		mhs.size_ingin = request.POST['singin']
		mhs.desc = request.POST['desc']
		mhs.kontak_wa = request.POST['wa']
		mhs.kontak_line = request.POST['line']
		mhs.kontak_ig = request.POST['ig']
		mhs.save()

	passed = {
		"mhs" : mhs,
		"ukuran":['S', 'M', 'L', 'XL'],
	}
	return render(request,"akun/user_profile.html",passed)

def edit_profile(request, username):
	if request.user.username == username:
	    if request.method == 'POST':
	        form = EditProfileForm(request.POST, instance=request.user)

	        if form.is_valid():
	            form.save()
	            return redirect('user/' + username)
	    else:
	        form = EditProfileForm(instance=request.user)
	        passed = {'form': form}
	        return render(request, 'akun/edit_profile.html', passed)
	else:
		return Http404('You are not authorized')