from django import forms
from django.contrib.auth.models import User
from aplikasiutama.models import Mahasiswa
from django.contrib.auth.forms import UserChangeForm

class EditProfileForm(UserChangeForm):
    template_name='/something/else'

    class Meta:
        model = Mahasiswa
        fields = [
            'size_punya',
            'size_ingin',
            'desc',
            'kontak_wa',
            'kontak_line',
            'kontak_ig',
        ]