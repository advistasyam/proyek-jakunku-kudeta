from django.shortcuts import render
from django.http import HttpResponse
from django.db.models import F
import json
from aplikasiutama.models import *
from .filters import MhsFilter

# Create your views here.

def dashboard(request):
	mhs = Mahasiswa.objects.filter(sudah_buat_thread=True)
	MyFilter = MhsFilter(request.GET, queryset=mhs)
	mhs = MyFilter.qs
	passed = {
		'mhs':mhs,
		'filter':MyFilter,
	}

	return render(request, 'dashboard/main.html', passed)

def search(request):
	return 

def tukar(request):
	user1 = request.user
	username = request.GET['req']
	user2 = User.objects.get(username=username)
	tukar = 1
	if Tukar.objects.filter(pengirim=user1, penerima=user2).exists():
		tukar = Tukar.objects.get(pengirim=user1, penerima=user2)
		tmp = tukar.status
		tukar.status = tmp ^ 1
	else:
		tukar = Tukar(pengirim=user1, penerima=user2, status=1)
	tukar.save()

	return HttpResponse('')



