import django_filters

from aplikasiutama.models import *


class MhsFilter(django_filters.FilterSet):
	class Meta:
		model = Mahasiswa
		fields = ['fakultas','size_punya','size_ingin']