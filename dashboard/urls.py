from django.urls import path
from . import views

app_name = 'dashboard'

urlpatterns = [
	path('', views.dashboard, name="dashboard"),
	path('search/', views.search),
	# path('buatthread/', views.buatthread, name="buatthread"),
	path('tukar/',views.tukar),
]