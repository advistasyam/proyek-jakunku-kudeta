from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Mahasiswa(models.Model):
    # img = models.ImageField(upload_to=,default=)
    user = models.OneToOneField(User, on_delete = models.CASCADE)
    fakultas = models.CharField(max_length=50)
    angkatan = models.CharField(max_length=3)
    size_punya = models.CharField(max_length=5)
    size_ingin = models.CharField(max_length=5)
    desc = models.TextField()
    sudah_tukar = models.BooleanField(default=False)
    sudah_buat_thread = models.BooleanField(default=False)
    kontak_wa = models.CharField(max_length=13, default='')
    kontak_line = models.CharField(max_length=15, default='')
    kontak_ig = models.CharField(max_length=20, default='')

    def __str__(self):
        return self.user.__str__()

class Tukar(models.Model):
    pengirim = models.ForeignKey(User, on_delete=models.CASCADE, related_name='%(class)s_sender')
    penerima = models.ForeignKey(User, on_delete=models.CASCADE, related_name='%(class)s_receiver')
    status = models.IntegerField(default=0)
    # 0 = default
    # 1 = sent          --->>> biar kalo klik buat cancel request tinggal xor aja jadi 0 atau 1
    # 2 = accept
    # 3 = reject

    def __str__(self):
        return self.pengirim.__str__() + ' - ' + self.penerima.__str__()
