from django.shortcuts import render, redirect
from django_sso_ui.decorators import with_sso_ui
import json
from django.http import HttpResponse
from django.contrib.auth.models import User, auth
from .models import *

# Create your views here.

def landing(request):
    if request.user.is_authenticated:
        passing_message = {
                'message' : "Woi jing lu angkatan " + request.user.last_name[:2] +" ngapain lagi nuker jakun",
        }
        return render(request, 'aplikasiutama/landing.html', passing_message)
    return render(request, 'aplikasiutama/landing.html')

@with_sso_ui(force_login=False)
def login(request, sso_profile):
    if User.objects.filter(username=lower(sso_profile["username"])).exists() :
        # print(sso_profile)
        user = auth.authenticate(username=lower(sso_profile["username"]), password=lower(sso_profile["attributes"]["kd_org"]))
        auth.login(request, user)
        # request.session.set_expiry(300) # auto logout dalam 5 menit
        return redirect("../../")
    else :
        user = User.objects.create_user(username=lower(sso_profile["username"]), password=lower(sso_profile["attributes"]["kd_org"]), first_name=sso_profile["attributes"]["ldap_cn"], last_name=sso_profile["attributes"]["npm"])
        user.save()
        mhs = Mahasiswa(user=user,fakultas=sso_profile['attributes']['faculty'],angkatan=sso_profile['attributes']['npm'][:2])
        mhs.save()
        user1 = auth.authenticate(username=lower(sso_profile["username"]), password=lower(sso_profile["attributes"]["kd_org"]))
        auth.login(request, user1)
        request.session.set_expiry(300) # auto logout dalam 5 menit
        return redirect("../../")

def logout(request):
    auth.logout(request)
    return redirect("../")
