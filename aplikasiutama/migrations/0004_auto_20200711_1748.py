# Generated by Django 3.0.6 on 2020-07-11 10:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aplikasiutama', '0003_kontak'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mahasiswa',
            name='kontak_line',
        ),
        migrations.RemoveField(
            model_name='mahasiswa',
            name='kontak_wa',
        ),
    ]
