# Generated by Django 3.0.6 on 2020-07-15 23:03

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('aplikasiutama', '0005_auto_20200712_1417'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tukar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('penerima', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='tukar_receiver', to=settings.AUTH_USER_MODEL)),
                ('pengirim', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='tukar_sender', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
